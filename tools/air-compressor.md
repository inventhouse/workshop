Air Compressor
==============

Air compressors range widely from huge expensive ones that are permanently installed and plumbed-in, down to tiny super-cheap tire inflators.  Air tools also vary a lot in the amount and pressure of air they require.

For my shop, I chose a reasonably portable one, quieter than some, with high enough pressure, tank size, and CFM ratings to be capable of running some moderately "thirsty" tools like an [HVLP paint gun](https://www.harborfreight.com/20-oz-hvlp-gravity-feed-air-spray-gun-62300.html) and keep recharge time short (compressors are always quietest when they're not pumping!)

[4 Gallon 200 PSI High Performance Hand Carry Jobsite Air Compressor](https://www.harborfreight.com/air-tools-compressors/air-compressors-tanks/4-gallon-200-psi-high-performance-hand-carry-jobsite-air-compressor-56339.html)

I knocked together a cart from scrap plywood and a [small furnature dolly](https://www.harborfreight.com/18-in-x-12-in-1000-lb-capacity-hardwood-dolly-63098.html) and even built a little organzer to store my [nailer/stapler](https://www.harborfreight.com/18-gauge-2-in-1-air-nailerstapler-68019.html) and other bits and parts.

![Air compressor cart front view](_assets/air-compressor-front-quarter.jpeg)
![Air compressor cart back view](_assets/air-compressor-back-quarter.jpeg)

---
