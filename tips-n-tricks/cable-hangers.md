Cable Hangers
=============

I needed to route some extension cords neatly along the ceiling joists to drop power to some parts of my shop; I could've used [romex staples](https://www.lowes.com/pd/Gardner-Bender-200-Count-1-2-in-Metal-Insulated-Cable-Staples/4633403), but here's what I came up with instead.

![Routed cable](_assets/cable-hangers-cord-route.jpeg)

1. I cut (on an angle) pieces of [hook-and-loop strap](https://www.harborfreight.com/3-4-quarter-inch-x-35-ft-roll-hook-and-loop-cable-strap-96215.html) about two hand-widths (6"-7") long and then cut them in half to make strips with one end angled and the other square.  Note that they're easier to peel apart if they stacked alternating like on the right-hand end of this picture: ![Stacked hanger strips](_assets/cable-hangers-strip-stack.jpeg)
2. Every 18" or so, I folded about a finger-width (3/4"-ish) of the square edge down and stapled them to the joist: ![Stapled strip](_assets/cable-hangers-closeup-open.jpeg)
3. Then I simply fastened them around the cable: ![Strip supporting cable](_assets/cable-hangers-closeup-closed.jpeg)

It's working _really_ well and if I ever need one of these cords temporarily for something else, it'd just be a few minutes to take it down and put it back.

---
