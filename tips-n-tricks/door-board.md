Door Board
==========

I've mounted an 18"x24" section of [whiteboard panel] next to the shop door; I'm using the top section for "to-do" projects and short-term goals, the lower section has shopping lists, and a little habit tracker in the corner.  The dividers and labels are written in Sharpie to be semi-permanent, but I can always erase and re-work them with the 90% rubbing alcohol in the little bottle above the board.  So far, it's working really well.

![Door board](_assets/door-board.jpeg)

[whiteboard panel]: https://www.lowes.com/pd/47-75-in-x-7-98-ft-Smooth-White-Wall-Panel/3015239

---
