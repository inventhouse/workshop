My Own Little Hardware Store
============================

Just a quick shop organization tip: I've spent a lot of time in a lot of different hardware stores over the years, and I've long found it helpful and natural to think about - and organize - my shop in those terms: I have a "paint department", a "plumbing section", "fasteners aisle", "electrical", "lumber", etc.  If I'm not sure where to put something, I ask myself "where would I find this in a hardware store?"

---
