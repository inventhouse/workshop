Workshop
========

_A space to document my workshop, and projects, improvements, tips-and-tricks_...

This is going to be a pretty free-form with folders and READMEs for categories and projects; very much a work-in-progress - like my shop.

I'm starting this because I'm fed-up with the state of my workshop; I got it off to a great start a couple years ago, but lately it's become so cluttered and disorganized that it's pretty unusable.  So follow along with my progress as I reorganize and build it out; I hope you learn some things, and I'll try to set up some way for folks to send me feedback and teach me things, too.

[Here's my plan for The Big Cleanup](big-cleanup/README.md)

- [Shop Tips-and-Tricks](tips-n-tricks/)
- [Tools in My Shop](tools/)

---
