Big Cleanup
===========

_My shop is unusable, here's what I'm doing about it_

### Recognize the Problem

I got frustrated by how much the condition of my shop was limiting me and slowing me down - and I've started actively nurturing that feeling of discontent.


### Gather Ideas

I spent about a week thinking about how to tackle the epic cleanup and reorganization I desperately needed to do and looking at ways others organize their shops; I watched most of [Adam Savage's'](https://en.wikipedia.org/wiki/Adam_Savage) [Shop Infrastructure playlist](https://www.youtube.com/playlist?list=PLJtitKU0CAegGi2QUH6ywt4EkjcR0jVam), especially looking at _how_ he optimizes and builds the things that make his little shop (much smaller than mine!) so incredibly effective - I want that!

### Make a Rough Plan

This is page is my plan


### Clear Enough Space to Work

Grand ideas are great, but I need usable space to make them happen; in the short term, it's ok to make other parts of the shop _worse_ to make one part good.  So I cleared and compacted an area near the back of the shop to put anything I don't immediately know what to do with; I call this The Pile.

This let me clear my heavy bench and the space around it _very_ quickly; this is a short-term solution, I will have to deal with this stuff eventually, but now I have space to work on more pressing things and, when it's time, to spread out a box-at-a-time and sort them.  I'm _Only_ clearing this one space to start, but guarding it fiercely; I'll tackle the other parts later.

![Main bench area before](_assets/main-bench-area-before.jpeg)![Main bench area after](_assets/main-bench-area-cleanup-1.jpeg)


### Invest in Acceleration

Spend a bit of time and/or money up-front on things that can help accelerate shop improvements; I [routed power drops](../tips-n-tricks/cable-hangers.md) to my large tools so I'm not constantly dragging an extension cord between them, cleared off the table saw, and (finally!) bought an air compressor and 18ga nailer/stapler.


### Deal with the Albatrosses

Finish or clean-up/organize some of the large projects that are cluttering the shop; I have a hammock stand, a rolling telescope cart, and a large dollhouse (to name a few) all in pieces all over the place.


### Build-Out Materials and Tools Organization

Round two of investing in acceleration; build better "first-order retrievability" storage for tools, fasteners, and lumber.  These, especially the tool organization system, should be flexible and modular (lots of ideas here, but I won't spend _too_ long noodling on designs - just build it!)


### Get Other Shop Areas to Good

Round two of clear enough space to work; get the other areas of the shop clear enough to be usable, pile stuff on The Pile.


### The Long Haul

Start overhauling the major organization systems, like the bin wall, and begin chipping away at The Pile, but also start _using_ the shop for other things.  I intend to start a [habit tracker](https://jamesclear.com/habit-tracker) to help me make progress in each of four categories every month: shop organization, shop infratructure, house project, and personal project.

---
